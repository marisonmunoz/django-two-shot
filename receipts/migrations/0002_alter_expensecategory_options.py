# Generated by Django 4.1.2 on 2022-10-19 21:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="expensecategory",
            options={"verbose_name_plural": "Expense Categories"},
        ),
    ]
